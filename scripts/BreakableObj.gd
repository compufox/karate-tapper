extends Node2D

onready var spr = $Sprite

var textures
var strength = 1
var above = null
var first = false

func _init_obj(txtrs, hp, pos):
	textures = txtrs
	position = pos
	strength = hp

func _ready():
	spr.set_texture(textures[0])

func _on_screen_exited():
	$"..".remove_child(self)
	queue_free()

func _on_ClickerArea_input_event(viewport, event, shape_idx):
	if (above == null or above.get_ref() == null) and \
	   event is InputEventMouseButton and event.is_pressed() and \
	   Singleton.Game.can_tap:
		strength -= 1
		
		if strength <= 0:
			Singleton.Game.tapped(first)
			spr.set_texture(textures[1])
			
			$"../Tween".interpolate_property(self, "position", position,
			$"..".to_local(to_global(Singleton.FinalPoint.position)), .14,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
			
			$"../Tween".start()

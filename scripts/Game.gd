extends Node2D

onready var TapLabel = $InGameUI/Base/Score
onready var TimeLabel = $InGameUI/Base/Timer
onready var PlusParticles = $TopBoard/Camera/Emitter
onready var Boards = $Boards

var board_obj = preload("res://scenes/BreakableObj.tscn")
var wood_plank = preload("res://Sprites/Plank.png")

export var board_cnt = 1
export(float) var time_limit = GlobalVars.TimeLimit
var current_time = 0
var can_tap = true
var taps = 0

signal round_over

func _ready():
	generate_boards(board_cnt)
	
	# pause to play the little text animations
	TimeLabel.set_text("Time Left: " + str(time_limit))
	get_tree().paused = true

func generate_boards(num):
	var initial_pos = $TopBoard.position
	var last_b = null
	for i in range(num):
		var pos = $TopBoard.position
		
		var b = board_obj.instance()
		b._init_obj([wood_plank, wood_plank], 1, Boards.to_local(pos))
		
		if last_b != null:
			last_b.get_ref().above = weakref(b)
		else:
			b.first = true
		
		Boards.add_child(b)
		
		randomize()
		$TopBoard.position += Vector2((randi() % 15) - 7, -10)
		last_b = weakref(b)
	$TopBoard/Camera.global_position.x = to_global(initial_pos).x

func _process(delta):
	if current_time < time_limit and can_tap:
		current_time = min(stepify(current_time + delta, 0.01), time_limit)
		
		TimeLabel.set_text("Time Left: " + str(time_limit - current_time))
	else:
		can_tap = false
		emit_signal("round_over")

func tapped(last = false):
	if can_tap:
		$TopBoard.position += Vector2(0, 10)
		taps += 1
		PlusParticles.emit()
		update_labels()
		
		can_tap = not last

func update_labels():
	if can_tap:
		TapLabel.set_text(str(taps + GlobalVars.Points))
